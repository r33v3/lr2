<?php
$a = rand(3,20);
$array = array();
$rez = array();
echo ("1. Массив из ".$a." элементов, заполненный случайными числами: ");
for ($i=0;$i<$a;$i++)
{
    $array[$i]=rand(10,99);
}
    echo implode("\r\n", $array);
echo '<br><br>'."2. Отсортированный массив: ";
sort($array);
    echo implode("\r\n", $array);
echo '<br><br>'."3. Массив в обратном порядке: ";
$rez = array_reverse($array);
    echo implode("\r\n", $rez);
echo '<br><br>'."4. Массив без последнего элемента: ";
array_pop($rez);
    echo implode("\r\n", $rez);
echo '<br><br>'."5. Сумма чисел в массиве: ";
$sum = array_sum($rez);
print_r($sum);
echo '<br><br>'."6. Количество элементов в массиве: ";
$count = count($rez);
print_r($count);
echo '<br><br>'."7. Среднее арифметическое для элементов массива: ";
$avg=$sum/$count;
print_r($avg);
if (in_array(50, $rez)) {
    echo '<br><br>'."8. Есть число 50";
} else {
    echo '<br><br>'."8. Нет числа 50";
}
echo '<br><br>'."9. Массив без повторяющихся значений: ";
$unique=array_unique($rez);
    echo implode("\r\n", $unique);
 ?>